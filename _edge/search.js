//import the search index as a json file
import searchIndex from "./search-index.json" assert { type: "json" };

//keep the version number of minisearch coming from the CDN
//in sync with the node package version that is used when
//building the search index
import MiniSearch from "https://cdn.jsdelivr.net/npm/minisearch@6.3.0/dist/es/index.js";

//create a new miniSearch instance with the search index
//and provide the configured index fields
//that have been attached to the search index json
//while building the index
const miniSearch = MiniSearch.loadJSON(JSON.stringify(searchIndex), {
  fields: searchIndex.INDEX_FIELDS,
});

//the search itself
//every Netlify edge function receives two arguments:
//the http request that was used for calling the function
//and a Netlify specific context object (in this case we are not using it)
export default async (request) => {
  const start = Date.now();

  const url = new URL(request.url);
  const searchParams = url.searchParams;
  const query = searchParams.get("query");
  try {
    let results = miniSearch.search(query, { prefix: true });
    const now = Date.now();
    console.log(
      `The search for [${query}] returned ${results.length} results within ${
        now - start
      } milliseconds`
    );
    //searches typically run within 1 millisecond
    return new Response(JSON.stringify(results), {
      status: 200,
      headers: { "content-type": "application/json;charset=UTF-8" },
    });
  } catch (error) {
    console.log(`Failure when searching for [${query}]: ${error}`);
    return new Response(error.message, {
      status: 500,
    });
  }
};
