const syntaxHighlight = require('@11ty/eleventy-plugin-syntaxhighlight')
const { DateTime } = require("luxon");
const markdownIt = require('markdown-it')
const markdownItAnchor = require('markdown-it-anchor')
const markdownItFootnote = require("markdown-it-footnote");
const markdownItAttrs = require("markdown-it-attrs");
const minisearch = require("./_eleventy/minisearch.js");
const striptags = require("striptags");

function extractExcerpt(article) {
  if (!article?.hasOwnProperty("templateContent")) {
    console.warn(
      'Failed to extract excerpt: Document has no property "templateContent".'
    );
    return null;
  }

  let excerpt = null;
  const content = article.data?.excerpt || article.templateContent;

  excerpt = striptags(content)
    .substring(0, 200) // Cap at 200 characters
    .replace(/^\s+|\s+$|\s+(?=\s)/g, "")
    .trim()
    .concat("...");

  return excerpt;
}

module.exports = function(eleventyConfig) {
  // Plugins
  eleventyConfig.addPlugin(syntaxHighlight)

  // To enable merging of tags
  eleventyConfig.setDataDeepMerge(true)

  // Copy these static files to _site folder
  eleventyConfig.addPassthroughCopy('src/assets')
  eleventyConfig.addPassthroughCopy('src/manifest.json')

  // To create a filter to determine duration of post
  eleventyConfig.addFilter('readTime', (value) => {
    const content = value
    const textOnly = content.replace(/(<([^>]+)>)/gi, '')
    const readingSpeedPerMin = 450
    return Math.max(1, Math.floor(textOnly.length / readingSpeedPerMin))
  })

  // Enable us to iterate over all the tags, excluding posts and all
  eleventyConfig.addCollection('tagList', collection => {
    const tagsSet = new Set()
    collection.getAll().forEach(item => {
      if (!item.data.tags) return
      item.data.tags
        .filter(tag => !['posts', 'all'].includes(tag))
        .forEach(tag => tagsSet.add(tag))
    })
    return Array.from(tagsSet).sort()
  })

  const options = {
    html: true, // Enable HTML tags in source
    breaks: true, // Convert '\n' in paragraphs into <br>
    linkify: true, // Autoconvert URL-like text to links
  };

  const md = markdownIt(options).use(markdownItFootnote).use(markdownItAttrs);

  md.use(markdownItAnchor, {
    level: [1, 2],
    permalink: markdownItAnchor.permalink.headerLink({
      safariReaderFix: true,
      class: 'header-anchor',
    })
  })
  eleventyConfig.setLibrary('md', md)

  // asset_img shortcode
  eleventyConfig.addShortcode('asset_img', (filename, alt) => {
    return `<img class="my-4" src="/assets/img/posts/${filename}" alt="${alt}" />`
  })

  eleventyConfig.addShortcode("markdown", (content) => md.render(content));

  eleventyConfig.addShortcode("excerpt", (article) => extractExcerpt(article));

  eleventyConfig.addFilter("postTags", (tags) => tags.filter(tag => !["posts", "search"].includes(tag)));

  eleventyConfig.addFilter("searchIndex", minisearch.searchIndex);

  eleventyConfig.addFilter("postDate", (dateObj) => {
    return DateTime.fromJSDate(dateObj).toLocaleString(DateTime.DATE_MED);
  });

  eleventyConfig.addCollection("liveContent", async (collectionAPI) => {
    //the content that is relevant for me
    //is inside of the 11ty input folder in content/**
    //- adjust this to your needs
    return collectionAPI.getFilteredByGlob(["content/**"]).filter((item) => {
      //the publish date must not be in the future
      //and frontmatter data should not have an entry of draft: true or draft: yes
      const now = new Date();
      return (
        item.date <= now &&
        item.data.draft !== true &&
        item.data.draft !== "yes"
      );
    });
  });

  return {
    markdownTemplateEngine: "njk",
    dir: {
      input: 'src',
      output: "public",
    }
  }
}
