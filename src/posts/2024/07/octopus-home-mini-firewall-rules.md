---
title: Octopus Home Mini firewall rules
date: 2024-07-25
# published: false
tags:
- nftables
- nixos
- iot
---
I recently acquired an [Octopus Home Mini](https://octopus.energy/blog/octopus-home-mini/), and wanted add it to a locked-down IoT wifi network I have at home.

This turned out to be pretty straightforward, I only needed three nftable rules:
```
iifname "iot" ip saddr HOME_MINI_IP_ADDRESS oifname "wan" udp dport { 123 } counter accept
iifname "iot" ip saddr HOME_MINI_IP_ADDRESS oifname "wan" tcp dport { 8883 } counter accept
iifname "wan" oifname "iot" ip daddr HOME_MINI_IP_ADDRESS ct state { established, related } counter accept
```

Where "wan" is the name of the internet interface, "iot" is the IoT interface (which is a VLAN), and HOME_MINI_IP_ADDRESS is the IP address set in DHCP for the home mini.
