---
layout: default
title: Tags
---

{% for tag in collections.tagList | postTags %}
<span>
  <a href="/tags/{{ tag }}"><button class="tag shadow">
    {{ tag }}
  </button>
  </a>
</span>
{% endfor %}
