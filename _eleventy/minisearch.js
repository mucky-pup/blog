const MiniSearch = require("minisearch");
const { DateTime } = require("luxon");
const striptags = require("striptags");

const formattedDate = (dateObj) => {
  return DateTime.fromJSDate(dateObj).toLocaleString(DateTime.DATE_MED);
};

const removeHtml = (text) => {
  if (text) {
    return striptags(text);
  }
};

const excerptFromText = (text) => {
  //return the first 25 words of the text
  let excerpt = removeHtml(text);
  if (excerpt) {
    let words = excerpt.split(" ");
    excerpt = words.slice(0, 25).join(" ");
    if (words.length > 25) {
      excerpt += " …";
    }
  }
  return excerpt;
};

const mapItem = (item) => {
  return {
    id: item.url,
    title: item.data.title,
    date: formattedDate(item.date),
    book: item.data.book,
    chapter: item.data.chapter,
    tags: item.data.tags.filter((item) => item !== "search"),
    content: removeHtml(item.templateContent),
  };
};

module.exports = {
  searchIndex: function (collection) {
    const INDEX_FIELDS = [
      "id", // The url of the page
      "title",
      "date",
      "content", // Index the content
    ];

    const STORE_FIELDS = [
      "id", // The url of the page
      "title",
      "date",
      "book",
      "tags",
      "chapter",
    ];

    let miniSearch = new MiniSearch({
      fields: INDEX_FIELDS,
      storeFields: STORE_FIELDS,
    });
    for (let item of collection) {
      let mappedItem = mapItem(item);
      if (mappedItem.id && !miniSearch.has(mappedItem.id)) {
        miniSearch.add(mappedItem);
      }
    }

    let searchIndex = miniSearch.toJSON();
    //store the configured index fields within the search index
    //to access it later when importing the index
    searchIndex.INDEX_FIELDS = INDEX_FIELDS;
    return JSON.stringify(searchIndex);
  },
};
