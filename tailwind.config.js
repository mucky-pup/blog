module.exports = {
  darkMode: 'class',
  content: ['./src/**/*.md', './src/**/*.html', './src/_includes/**/*.njk'],
  theme: {
    extend: {
      colors: {
        'lighter': '#E6CBB1',
        'darker': '#4D3D3A',
        'darkest': '#170902',
      }
    }
  },
  plugins: [
    require('@tailwindcss/typography')
  ],
}
